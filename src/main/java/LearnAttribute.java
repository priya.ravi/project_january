import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class LearnAttribute {
	public class TC001_EditLead extends SeleniumBase{

		@Test(invocationCount = 2, timeOut = 30000)
		public void login() {
			startApp("chrome","http://leaftaps.com/opentaps");
			WebElement eleUsername = locateElement("id", "username");
			clearAndType(eleUsername, "DemoSalesManager"); 
			WebElement elePassword = locateElement("id", "password");
			clearAndType(elePassword, "crmsfa"); 
			WebElement eleLogin = locateElement("class", "decorativeSubmit");
			click(eleLogin); 
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			//click(eleLogout);
			WebElement crm = locateElement("LinkText","CRM/SFA"); 
			click(crm);
			WebElement createlead = locateElement("LinkText","Create Lead");	
			click(createlead);
			WebElement name = locateElement("id","createLeadForm_companyName");
			clearAndType(name, "Vishnu");
			WebElement name1 = locateElement("id","createLeadForm_firstName");
			clearAndType(name1, "priya");
			WebElement name2 = locateElement("id","createLeadForm_lastName");
			clearAndType(name2, "R");
			WebElement Create = locateElement("class", "smallSubmit");
			click(Create);
		}	
}
}