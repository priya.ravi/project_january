package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC003_CreateLEad extends SeleniumBase{
	

@Test(dataProvider = "getdata")
public void createLEad(String cname, String fname, String lname) {
	
	click(locateElement("id", "Leads"));
	click(locateElement("id", "CreateLeads"));
	clearAndType(locateElement("id", "CreateLeadForm_companyName"),cname);
	clearAndType(locateElement("id", "CreateLeadForm_firstName"),fname);
	clearAndType(locateElement("id", "CreateLeadForm_lastName"),lname);
	click(locateElement("name", "submitButton"));
		
}

@DataProvider(name = "getdata")
public String[][] fetchdata() {
	String [][] data = new String[2][3];
	data[0][0]="TestLeaf";
	data[0][1]="priya";
	data[0][2]="R";
	
	data[0][0]="TestLeaf";
	data[0][1]="Abi";
	data[0][2]="R";
	return data;
}













	
}








