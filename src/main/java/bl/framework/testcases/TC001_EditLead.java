package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC001_EditLead extends SeleniumBase{

	@Test(invocationCount = 2)
	public void login() {
		startApp("chrome","http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLogout);
	
		WebElement crm = locateElement("LinkText","CRM/SFA"); 
		click(crm);
	WebElement createlead = locateElement("LinkText","Create Lead");	
	click(createlead);
	WebElement name = locateElement("id","createLeadForm_companyName");
	clearAndType(name, "Vishnu");
	WebElement name1 = locateElement("id","createLeadForm_firstName");
	clearAndType(name1, "priya");
	WebElement name2 = locateElement("id","createLeadForm_lastName");
	clearAndType(name2, "R");
	WebElement Create = locateElement("class", "smallSubmit");
	click(Create); 
	WebElement Edit = locateElement("class","subMenuButton");
	click(Edit);
	WebElement name3 = locateElement("id","createLeadForm_companyName");
	clearAndType(name3, "Priya");
	}
//	public void editLead() throws InterruptedException {
//	login();
//		WebElement eleLeads = locateElement("xpath", "//a[contains(text(),'Leads')]");
//		click(eleLeads);
//		WebElement eleFindLead = locateElement("xpath", "//a[contains(text(),'Find Leads')]");
//		click(eleFindLead);
//	   WebElement eleFirstName =locateElement("xpath","//div[@class='x-form-item x-tab-item']//input[@name='firstName']");
//       clearAndType (eleFirstName, "Mathivathani");
//        WebElement eleFindLeadsCheck = locateElement("xpath", "//button[contains(text(),'Find Leads')]");
//    	click(eleFindLeadsCheck);
//    	WebElement eleFirstLeadsCheck = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a[@class='linktext'][1]");
//    	click(eleFirstLeadsCheck);
//    	boolean verifyTitle = verifyTitle("View Lead");
//    	if(verifyTitle) {
//    		System.out.println("Titleis verified");
//    	}
//    	else {
//    		System.out.println("Title is incorrect");
//    	}
//    	WebElement eleEditLead = locateElement("xpath", "//a[contains(tex(),'Edit')]");
//    	click(eleEditLead);
//    	WebElement eleCompName = locateElement("id","updateLeadForm_companyName");
//        String updCompName="WIPRO";clearAndType(eleCompName, updCompName);
//        WebElement eleUpdateButton = locateElement("xpath", "//input[@value='Update']");
//    	click(eleUpdateButton);
//    	WebElement eleCompNameUpd = locateElement("id", "viewLead_companyName_sp");
//    	boolean verifyText = verifyPartialText(eleCompNameUpd, updCompName);
//    	if(verifyText) {
//    		System.out.println("Updated Company Name is correct");
//    	}
//    	else {
//    		System.out.println("Updated Company Name is incorrect");
//    	
}
         
    	
    	
	
