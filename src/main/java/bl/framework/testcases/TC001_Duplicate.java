package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC001_Duplicate extends SeleniumBase{

	private WebElement webElement;
	@Parameters({"url","username","password"})
	@Test 
	public void login(String url, String uname, String pwd) {
		startApp("chrome", url);
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, uname); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, pwd); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLogout);
	
		WebElement crm = locateElement("LinkText","CRM/SFA"); 
			click(crm);
		WebElement createlead = locateElement("LinkText","Create Lead");	
		click(createlead);
		WebElement name = locateElement("id","createLeadForm_companyName");
		clearAndType(name, "Vishnu");
		WebElement name1 = locateElement("id","createLeadForm_firstName");
		clearAndType(name1, "priya");
		WebElement name2 = locateElement("id","createLeadForm_lastName");
		clearAndType(name2, "R");
		WebElement Create = locateElement("class", "smallSubmit");
		click(Create); 
	
	}
	
}








