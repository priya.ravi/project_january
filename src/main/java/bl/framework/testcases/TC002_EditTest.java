package bl.framework.testcases;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC002_EditTest extends SeleniumBase {
	@Test (groups = "sanity")
	public void EditTest(){
		login();	
	

	WebElement eleLeads = locateElement("xpath", "//a[contains(text(),'Leads')]");
	click(eleLeads);
	WebElement eleFindLead = locateElement("xpath", "//a[contains(text(),'Find Leads')]");
	click(eleFindLead);
    WebElement eleFirstName = locateElement("xpath", "//div[@class='x-form-item x-tab-item']//input[@name='firstName']");
    clearAndType(eleFirstName, "Mathivathani");
    WebElement eleFindLeadsCheck = locateElement("xpath", "//button[contains(text(),'Find Leads')]");
	click(eleFindLeadsCheck);
	WebElement eleFirstLeadsCheck = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a[@class='linktext'][1]");
	click(eleFirstLeadsCheck);
	boolean verifyTitle = verifyTitle("View Lead");
}

	private void login() {
		// TODO Auto-generated method stub
		
	}
}