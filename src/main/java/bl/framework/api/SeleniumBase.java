package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bl.framework.base.Browser;
import bl.framework.base.Element;

public class SeleniumBase implements Browser, Element{

	public RemoteWebDriver driver;
	public int i =1;
	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startApp(String browser, String url) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver(); 
		}
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The browser "+browser+" launched successfully");
        takeSnap();
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
	try
	{
	switch (locatorType) {
		case "id": return driver.findElementById(value);
		case "name": return driver.findElementByName(value);
		case "class": return driver.findElementByClassName(value);
		case "xpath": return driver.findElementByXPath(value);
		case "LinkText": return driver.findElementByLinkText(value);
		
		default:
			break;
		}
	logStep("Located the element using given locator type "+locatorType+" successfully","pass");
	}
	catch (NoSuchElementException e)
	{
			logStep("No such element is found.Please check the locator!!.","fail");
			takeSnap();
		e.printStackTrace();
	}
		return null;
	}

	private void logStep(String string, String string2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		try {
			driver.findElementById(value);
			logStep("The located element using Id with "+value+" is found successfully","pass");
			takeSnap();
		} catch (NoSuchElementException e) {
			logStep("The element using Id with "+value+" is not found","fail");
			e.printStackTrace();
			takeSnap();
		}
		catch (WebDriverException e) {
			logStep("Please check the element with correct "+value+" in DOM","fail");
			e.printStackTrace();
			takeSnap();
		}
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		// TODO Auto-generated method stub
		try {
			switch(type) {
			case "id": return driver.findElementsById(value);
			case "name":return driver.findElementsByName(value);
			case "class": return driver.findElementsByClassName(value);
			case "xpath": return driver.findElementsByXPath(value);
			case "linkText":return driver.findElementsByLinkText(value);
			case "cssSelector": return driver.findElementsByCssSelector(value);
			case "partialLinkText": return driver.findElementsByPartialLinkText(value);
			case "tagName": return driver.findElementsByTagName(value);
			default:
				break;
			}
			logStep("located the element using given locator type "+type+" successfully","pass");
		} catch (NoSuchElementException e) {
			logStep("No such element is found.Please check the locator","fail");
			takeSnap();
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void switchToAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert();
			logStep("Switched to the alert successfully","pass");
		} catch (NoAlertPresentException e) {
			logStep("No Alert is present in current location.Please check the code!!","fail");
			e.printStackTrace();
		}
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
			logStep("Accepted the Alert successfully","pass");
		}
		catch (NoAlertPresentException e) {
			logStep("No Alert present in current location.Please check the code","fail");
			e.printStackTrace();
		}
		catch (UnhandledAlertException e) {
			logStep("Alert is unhandled.Please accept/dismiss the alert","fail");
			e.printStackTrace();
		}
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().dismiss();
			logStep("Dismissed the alert successfully","pass");
		} catch (NoAlertPresentException e) {
			logStep("No Alert present in current location.Please check the code!!","fail");
			e.printStackTrace();
		}
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		try {
			String text = driver.switchTo().alert().getText();
			logStep("The text present in the alert is "+text,"pass");
		} catch (NoAlertPresentException e) {
			logStep("No Alert present in current location.Please check the code","fail");
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public void typeAlert(String data) {
		// TODO Auto-generated method stub
		{
			try {
				driver.switchTo().alert().sendKeys(data);
				logStep("Entered the "+data+" in the alert successfully","pass");
			} catch (NoAlertPresentException e) {
				logStep("No Alert present in current location.Please check the code","fail");
				e.printStackTrace();
			}
		}

	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		try {
			Set<String> windowHandles = driver.getWindowHandles();
			List<String>ls=new ArrayList<>(windowHandles);
			driver.switchTo().window(ls.get(index));
			logStep("Switched to the window using the given index "+index+" successfully","pass");
			takeSnap();
		} catch (NoSuchWindowException e) {
			logStep("There's no window present.Please check the code!!","fail");
			e.printStackTrace();
			takeSnap();
		}
	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub
		try {
			Set<String> windowHandles = driver.getWindowHandles();
			List<String>ls=new ArrayList<>(windowHandles);
			System.out.println("The number of windows present are: "+ls.size());
			driver.switchTo().window(title);
			logStep("Switched to correct window using "+title+" successfully","pass");
			takeSnap();
		} catch (NoSuchWindowException e) {
			logStep("No window found in current location.Please check the code","fail");
			takeSnap();
			e.printStackTrace();
		}
	}

	@Override
	public void switchToFrame(int index) {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().frame(index);
			logStep("Switched to correct frame using index as "+index+ " successfully","pass");
			takeSnap();
		} catch (NoSuchFrameException e) {
			logStep("No such frame is found.Please check the index in the code!!","fail");
			takeSnap();
			e.printStackTrace();
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().frame(ele);
			logStep("Switched to correct frame using the element "+ele+ " successfully","pass");
			takeSnap();
		} catch (NoSuchFrameException e) {
			logStep("No such frame is found.Please check the element in the code","fail");
			takeSnap();
			e.printStackTrace();
		}
		catch (StaleElementReferenceException e) {
			logStep("The Frame you're looking for is currently not in DOM.Please check the code","fail");
			takeSnap();
			e.printStackTrace();
		}
	}

	@Override
	public void switchToFrame(String idOrName) {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().frame(idOrName);
			logStep("Switched to correct frame using id/Name "+idOrName+ " successfully","pass");
			takeSnap();
		} catch (NoSuchFrameException e) {
			logStep("No such frame is found.Please check the idOrName in the code","fail");
			takeSnap();
			e.printStackTrace();
		}
	}

	@Override
	public void defaultContent() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().defaultContent();
			logStep("Switched to the default content or Frame successfully","pass");
			takeSnap();
		} catch (NoSuchFrameException e) {
			logStep("No such frame is found","fail");
			takeSnap();
			e.printStackTrace();
		}
	}

	@Override
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		String currentUrl = driver.getCurrentUrl();
		if(currentUrl.equals(url)) {
			logStep("The current url "+url+" matches with expected successfully","pass");
			takeSnap();
			return true;
		}
		else {
			logStep("Failed to match the current url with expected","fail");
			return false;
	}
	}

	@Override
	public boolean verifyTitle(String title) {
		// TODO Auto-generated method stub
		{
			String expectedTitle=driver.getTitle();
			if(title.contains(expectedTitle)) {
				logStep("The title "+title+" is getting displayed as expected","pass");
				takeSnap();
				return true;
			}
			else {
				logStep("The title "+title+" is not displayed correctly as expected","fail");
				takeSnap();
				return false;
			}
		}
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		driver.close();
		logStep("Active Browser is closed successfully","pass");
	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub
		driver.close();
		logStep("Active Browser is closed successfully","pass");
	}

	@Override
	public void click(WebElement ele) {
		try {
		ele.click();
		System.out.println("The element "+ele+" clicked successfully");
		takeSnap();
	} 
	catch (NoSuchElementException e) {
		logStep("Unable to click the element","fail");
		takeSnap();
		e.printStackTrace();
	}
	}

	@Override
	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub
		try {
			ele.sendKeys(Keys.ARROW_RIGHT);
			ele.sendKeys(data);
			logStep("The given data "+data+" is appended to the element "+ele+" successfully","pass");
			takeSnap();
		} catch (ElementNotInteractableException e) {
			logStep("Element is present in DOM and not able to interact","fail");
			takeSnap();
			e.printStackTrace();     
		}
		catch (IllegalArgumentException e) {
			logStep("Please check the argument that's passed","fail");
			takeSnap();
			e.printStackTrace();
		}
	}

	@Override
	public void clear(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			ele.clear();
			logStep("Cleared the value in given text field successfully","pass");
			takeSnap();
		} catch (InvalidElementStateException e) {
			logStep("The specified element is non-editable.Please check the element","fail");
			takeSnap();
			e.printStackTrace();
		}
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		ele.clear();
		ele.sendKeys(data); 
		System.out.println("The data "+data+" entered successfully");
		takeSnap();
	}

	@Override
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			String text = ele.getAttribute("value");
			logStep("The captured element text is "+text,"pass");
			takeSnap();
			return text;
		} catch (StaleElementReferenceException e) {
			logStep("Element is not attached to the DOM.Please check","fail");
			takeSnap();
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			String bgColor = ele.getCssValue("background-color");
			logStep("The background color of the given element " + ele + " is " + bgColor,"pass");
			takeSnap();
		} catch (Exception e) {
			logStep("Failed to get the background color of the given element","fail");
			takeSnap();
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			String typedText = ele.getText();
			logStep("The typed text is "+typedText,"pass");
			takeSnap();
		} catch (WebDriverException e) {
			logStep("Please check the element","fail");
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		try {
			Select selectUsingText=new Select(ele);
			selectUsingText.selectByVisibleText(value);
			logStep("The given text "+value+" is selected in dropdown successfully","pass");
			takeSnap();
		} catch (NoSuchElementException e) {
			logStep("Element is not present as dropdown","fail");
			e.printStackTrace();
			takeSnap();
		}catch (ElementNotVisibleException e) {
			logStep("Element is not visible as dropdown","fail");
			e.printStackTrace();
			takeSnap();
		}
		catch (WebDriverException e) {
			logStep("Please check the element","fail");
			e.printStackTrace();
			takeSnap();
		}
		finally {
			//logStep("finally block is executed","warning");
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		try {
			Select selectUsingIndex=new Select(ele);
			selectUsingIndex.selectByIndex(index);
			logStep("The given value "+index+" is selected in dropdown successfully","pass");
			takeSnap();
		} catch (NoSuchElementException e) {
			logStep("Element is not present as dropdown","fail");
			e.printStackTrace();
			takeSnap();
		}catch (ElementNotVisibleException e) {
			logStep("Element is not visible as dropdown","fail");
			e.printStackTrace();
			takeSnap();
		}
		catch (WebDriverException e) {
			logStep("Please check the element","fail");
			e.printStackTrace();
			takeSnap();
		}
		finally {
			//System.out.println("finally block is executed");
			takeSnap();
		}
		}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		try {
			Select selectByValue=new Select(ele);
			selectByValue.selectByValue(value);
			logStep("The given value "+value+" is selected in dropdown successfully","pass");
			takeSnap();
		} catch (NoSuchElementException e) {
			logStep("Element is not present as dropdown","fail");
			e.printStackTrace();
			takeSnap();
		}catch (ElementNotVisibleException e) {
			logStep("Element is not visible as dropdown","fail");
			e.printStackTrace();
			takeSnap();
		}
		catch (WebDriverException e) {
			logStep("Please check the element","fail");
			e.printStackTrace();
			takeSnap();
		}
		finally {
			//System.out.println("finally block is executed");
			takeSnap();
		}
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		if(ele.getText().equals(expectedText)) {
			logStep("Exact Text "+expectedText+" is getting displayed correctly","pass");
			takeSnap();
		}
		else {
			logStep("Exact text "+expectedText+" is not getting displayed","fail");
			takeSnap();
			return false;
		}
		return true;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		if(ele.getText().contains(expectedText)) {
			logStep("Expected partial text "+expectedText+" is getting displayed successfully","pass");
			takeSnap();
			return true;
		}
		else {
			logStep("Expected partial text "+expectedText+" is not getting displayed","fail");
			takeSnap();
			return false;
		}
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		String attributeValue = ele.getAttribute(attribute);
		if(attributeValue.equals(value)) {
			logStep("The attribute "+attribute+ "matches exactly with given value "+value+" successfully","pass");
			takeSnap();
			return true;
		}
		else {
			logStep("The attribute "+attribute+ " doesn't matches with given value "+value,"fail");
			takeSnap();
			return false;
		}
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
			String attributeValue = ele.getAttribute(attribute);
			if(attributeValue.contains(value)) {
				logStep("The attribute "+attribute+ "partially matches exactly with given value "+value+" successfully","pass");
				takeSnap();
			}
		} catch (Exception e) {
			logStep("The attribute "+attribute+ "doesn't matches partially with given value "+value,"fail");
			takeSnap();	
			e.printStackTrace();
		}
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		boolean displayed = ele.isDisplayed();
		if(displayed) {
			logStep("The element "+ele+" is visible successfully","pass");
			takeSnap();
			return true;
		}
		else {
			logStep("The element "+ele+" is not visible successfully","fail");
			takeSnap();
			return false;
		}
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		boolean displayed = ele.isDisplayed();
		if(!displayed) {
			logStep("The element "+ele+" is not visible and disappeared successfully","pass");
			takeSnap();
			return true;
		}
		else {
			logStep("The element "+ele+" is visible and not disappeared","fail");
			takeSnap();
			return false;
		}
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		boolean enabled = ele.isEnabled();
		if(enabled) {
			logStep("The element "+ele+" is enabled successfully","pass");
			takeSnap();
			return true;
		}
		else {
			logStep("The element "+ele+" is not enabled","fail");
			takeSnap();
			return false;
		}
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		boolean selected = ele.isSelected();
		if(selected) {
			logStep("The element "+ele+" is selected successfully","pass");
			takeSnap();
			return true;
		}
		else {
			logStep("The element "+ele+" is not selected","fail");
			takeSnap();
			return false;
		}
	}

}
