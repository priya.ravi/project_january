

	import java.util.ArrayList;
	import java.util.List;
	import java.util.Set;
	import java.util.concurrent.TimeUnit;

	import org.openqa.selenium.Alert;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;

	public class MergeLeads {

		public static void main(String[] args) throws InterruptedException {
			System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
			ChromeDriver driver = new ChromeDriver();
			driver.manage().window().maximize();

			// launch url
			driver.get("http://leaftaps.com/opentaps/control/login");
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			// enter user name
			driver.findElementById("username").sendKeys("DemoSalesManager");
			// enter password
			driver.findElementById("password").sendKeys("crmsfa");
	//click login
			driver.findElementByClassName("decorativeSubmit").click();
			// Click crm/sfa link
			driver.findElementByLinkText("CRM/SFA").click();
			// Click Leads link

			driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();
			// clicking Merge Leads
			driver.findElementByXPath("//a[text()='Merge Leads']").click();
			// clicking icon near From Lead
			driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
			Set<String> windowHandles = driver.getWindowHandles();
			List<String> ls = new ArrayList<>();
			ls.addAll(windowHandles);

			driver.switchTo().window(ls.get(1));
			driver.findElementByXPath("//input[@name='id']").sendKeys("10029");
			driver.findElementByXPath("//button[text()='Find Leads']").click();
			Thread.sleep(3000);
			driver.findElementByXPath("(//table[@class='x-grid3-row-table']//a)[1]").click();
			driver.switchTo().window(ls.get(0));
			Thread.sleep(3000);
			driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
			Thread.sleep(3000);
			windowHandles = driver.getWindowHandles();
			List<String> ls1 = new ArrayList<>();
			ls1.addAll(windowHandles);
			driver.switchTo().window(ls1.get(1));
			driver.findElementByXPath("//input[@name='id']").sendKeys("10035");
			driver.findElementByXPath("//button[text()='Find Leads']").click();
			Thread.sleep(3000);
			driver.findElementByXPath("(//table[@class='x-grid3-row-table']//a)[1]").click();
			driver.switchTo().window(ls1.get(0));
			driver.findElementByXPath("//a[text()='Merge']").click();
			driver.switchTo().alert().accept();
			driver.findElementByLinkText("Find Leads").click();
			Thread.sleep(2000);
			driver.findElementByXPath("//input[@name='id']").sendKeys("20000");
			driver.findElementByXPath("//button[text() = 'Find Leads']").click();
			WebElement message = driver.findElementByXPath("//div[text()='No records to display']");
			if(message.isDisplayed())
			{
				System.out.println("No records to display as expected");
			}
			else {
				System.out.println("Message is not displayed");
			}
			

		}

	}


