package utils;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReports {
	ExtentHtmlReporter Html;
	ExtentReports extent;
	ExtentTest test;
	@Test
	public void runreport() {
		Html = new ExtentHtmlReporter("./report/extentReport.html");
		extent = new ExtentReports();
		Html.setAppendExisting(true);
		extent.attachReporter(Html);
		test = extent.createTest("Tc001_Login","Login into leaftaps");
		test.assignAuthor("Priya");
		test.assignCategory("regression");
		try {
			test.fail("username demosales manager entered succesfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//	extent.flush();

	}
	}
