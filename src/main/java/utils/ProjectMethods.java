package utils;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import bl.framework.api.SeleniumBase;

//import bl.framework.api.SeleniumBase;

public class ProjectMethods extends SeleniumBase {
	
	//public String testCaseName,testDescription,author,category;
	
		@BeforeSuite/*(groups="common")*/
		public void startExtentReports() {
			startExtentReports();
		}
		@AfterSuite/*(groups="common")*/
		public void closeExtentReports() {
			endReports();
		}
		private void endReports() {
			// TODO Auto-generated method stub
			
		}
		@BeforeClass/*(groups="common")*/
		public void beforeClass() {
			initializeReports();
		}
		
		private void initializeReports() {
			// TODO Auto-generated method stub
			
		}
		@Parameters({"url","username","password"})
		@BeforeMethod
		public void login(String url, String usname, String pword) {
			startApp("chrome", url);
			WebElement elUserName = locateElement("id", "username");
			clearAndType(elUserName, usname);
			WebElement elPassword = locateElement("id", "password");
			clearAndType(elPassword, pword);
			WebElement eleClick = locateElement("class", "decorativeSubmit");
			click(eleClick);
			WebElement clickLink = locateElement("linkText", "CRM/SFA");
			click(clickLink);
		}
		
		//@AfterMethod
		public void closeBrowser() {
			close();
			//quit();
		}
	}