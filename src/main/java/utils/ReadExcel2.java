package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel2 {

	public static Object[][] readData1(String dataSheetName) {
		//Enter workbook
		XSSFWorkbook wbook;
		//Enter Sheet
		
		try {
			wbook = new XSSFWorkbook("./Data/"+dataSheetName);	
		XSSFSheet sheet = wbook.getSheet("sheet1");
		//Getting cell count for header row count
		
		int rowcount = sheet.getLastRowNum();
		System.out.println("Row count" +rowcount);
		//Getting cell count for header column count
		int colcount = sheet.getLastRowNum();
		System.out.println("Row count"+colcount);
		Object[][] data = new Object[rowcount][colcount];
		for (int i = 1; i <= rowcount; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j <= colcount; j++) {
				XSSFCell cell = row.getCell(j);
				data[i-1][j] = cell.getStringCellValue();
			}
			}
		}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return null;
	}
}
			
			
